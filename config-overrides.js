const {
  override,
  fixBabelImports,
  addLessLoader,
  addWebpackAlias,
} = require("customize-cra");
const path = require("path");
function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = override(
  fixBabelImports("import", {
    libraryName: "antd",
    libraryDirectory: "es",
    style: true
  }),

  addLessLoader({
    javascriptEnabled: true,
    modifyVars: { "@primary-color": "#fd961a" },
  }),

  // 配置路径别名
  addWebpackAlias({
    "@": resolve("src"),
  }),
);
