import * as types from "../action-types";
import { getToken } from "@/utils/auth";
const initUserInfo = {
  account: "",
  name: "",
  roles: [],
  email: "",
  phone: "",
  userId: "",
  status: "",
  partner: {},
  token: getToken()
};
export default function user(state = initUserInfo, action) {
  switch (action.type) {
    case types.USER_SET_USER_TOKEN:
      return {
        ...state,
        token: action.token
      };
    case types.USER_SET_USER_INFO:
      return {
        ...state,
        account: action.account,
        name: action.name,
        roles: action.roles,
        email: action.email,
        phone: action.phone,
        userId: action.userId,
        status: action.status,
        partner: action.partner
      };
    case types.USER_RESET_USER:
      return initUserInfo;
    default:
      return state;
  }
}
