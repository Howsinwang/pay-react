import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import cn from '../assets/lang/cn.json';
import tw from '../assets/lang/tw.json';
import en from '../assets/lang/en.json';

const resources = {
  'zh-CN': {
    translation: cn
  },
  'zh-TW': {
    translation: tw
  },
  'en': {
    translation: en
  }
};

i18n.use(initReactI18next).init({
  resources,
  lng: 'zh-CN', // 預設語言
  fallbackLng: 'zh-TW', // 如果當前切換的語言沒有對應的翻譯則使用這個語言，
  interpolation: {
    escapeValue: false
  }
});

export default i18n;
