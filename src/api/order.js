import request from '@/utils/request'

export function getRechargeOrder() {
  return request({
    url: '/order/recharge',
    method: 'get'
  })
}

export function getWithdrawOrder() {
  return request({
    url: '/order/withdraw',
    method: 'get'
  })
}

export function getDepositOrder() {
  return request({
    url: '/order/deposit',
    method: 'get'
  })
}

export function getCollect() {
  return request({
    url: '/order/collect',
    method: 'get'
  })
}

export function getDeposit(params) {
  return request({
    url: '/api/deposit',
    method: 'get',
    params
  })
}

export function confirmPay(data) {
  console.log(data);
  return request({
    url: '/api/deposit',
    method: 'put',
    data
  })
}

export function cancelOrder(data) {
  return request({
    url: '/api/deposit',
    method: 'delete',
    data
  })
}

export function callbackOrder(data) {
  return request({
    url: '/order/callback',
    method: 'put',
    data
  })
}

export function replenishOrder(data) {
  return request({
    url: '/order/replenish',
    method: 'put',
    data
  })
}

export function reverseOrder(data) {
  return request({
    url: '/order/reverse',
    method: 'put',
    data
  })
}

export function getGamblingOrder() {
  return request({
    url: '/order/gambling',
    method: 'get'
  })
}