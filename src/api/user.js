import request from '@/utils/request'

export function reqUserInfo(data) {
  return request({
    url: '/user/info',
    method: 'post',
    data
  })
}

export function getUsers() {
  return request({
    url: '/user/user',
    method: 'get'
  })
}

export function deleteUser(data) {
  return request({
    url: '/user/delete',
    method: 'post',
    data
  })
}

export function editUser(data) {
  return request({
    url: '/user/edit',
    method: 'post',
    data
  })
}

export function reqValidatUserID(data) {
  return request({
    url: '/user/validatUserID',
    method: 'post',
    data
  })
}

export function addUser(data) {
  return request({
    url: '/user/add',
    method: 'post',
    data
  })
}

export function changeNotifyUrl(data) {
  return request({
    url: '/user/changeNotifyUrl',
    method: 'post',
    data
  })
}

export function changeCollectWallet(data) {
  return request({
    url: '/user/changeCollectWallet',
    method: 'post',
    data
  })
}

export function changePassword(data) {
  return request({
    url: '/user/changePassword',
    method: 'post',
    data
  })
}

export function checkGoogleCode() {
  return request({
    url: '/user/google/check',
    method: 'get'
  })
}

export function getGoogleCode() {
  return request({
    url: '/user/google/code',
    method: 'get'
  })
}

export function saveGoogleCode(data) {
  return request({
    url: '/user/google/save',
    method: 'post',
    data
  })
}

export function verifyGoogleCode(data) {
  return request({
    url: '/user/google/verify',
    method: 'post',
    data
  })
}

export function updatePartnerExchangeRate(data) {
  return request({
    url: '/user/partner/rate',
    method: 'put',
    data
  })
}

export function partnerWithdraw(data) {
  return request({
    url: '/user/withdraw',
    method: 'post',
    data
  })
}
