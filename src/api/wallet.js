import request from '@/utils/request'

export function getWallets(walletType) {
  return request({
    url: `/wallet/${walletType}`,
    method: 'get'
  })
}

export function updateWallet(walletType,data) {
  return request({
    url: `/wallet/${walletType}`,
    method: 'patch',
    data
  })
}

export function createWallet(walletType,data) {
  return request({
    url: `/wallet/${walletType}`,
    method: 'post',
    data
  })
}

export function deleteWallet(walletType,data) {
  return request({
    url: `/wallet/${walletType}`,
    method: 'delete',
    data
  })
}
