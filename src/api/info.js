import request from '@/utils/request'

export function getInfo(data) {
  return request({
    url: '/info/total',
    method: 'post',
    data
  })
}

export function getWalletInfo(data) {
  return request({
    url: '/info/wallet',
    method: 'post',
    data
  })
}

export function getBalanceFlow() {
  return request({
    url: '/info/balance',
    method: 'get'
  })
}