import React, { useEffect, useState, useCallback } from "react";
import { connect } from "react-redux"
import { Table, Card, Modal, Descriptions, DatePicker, Form, Input, Select, message } from "antd";
import OperationButton from "@/components/OperationButton";
import StatusTag from "@/components/StatusTag";
import { getRechargeOrder } from "@/api/order";
import "./index.less"
import { useTranslation } from 'react-i18next';
const { RangePicker } = DatePicker;
const RechargeOrder = (props) => {
    const { t } = useTranslation();
    const columns = [
        {
            title: t("order.orderId"),
            dataIndex: 'orderId',
            render: orderId => `${orderId.substring(0, 15)}...`,
            width: '15%',
            key: 'orderId',
        },
        {
            title: t("order.amount"),
            dataIndex: 'amount',
            key: 'amount',
        },
        {
            title: t("order.chain"),
            dataIndex: 'chain',
            key: 'chain',
        },
        {
            title: t("order.handlingFee"),
            dataIndex: 'handlingFee',
            key: 'handlingFee',
        },
        {
            title: t("order.status"),
            dataIndex: 'status',
            key: 'status',
            render: (status) => <StatusTag status={status}/>
        },
        {
            title: t("order.createdDate"),
            dataIndex: 'createdDate',
            key: 'createdDate',
            render: createdDate => new Date(createdDate).toLocaleString(),
            defaultSortOrder: 'descend',
            sorter: (a, b) => (
                new Date(a.createdDate).getTime() - new Date(b.createdDate).getTime()
            )
        },
        {
            title: t("operation"),
            render: (order) => (
                <OperationButton 
                    order={order} 
                    operation={{
                        detail : showModal,
                        callback : update
                    }}
                />
            )
        }
    ];
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [condition, setCondition] = useState({});
    const [loading, setLoading] = useState(false);
    const [order, setOrder] = useState({});
    const [result, setResult] = useState([]);

    const fetchData = useCallback((condition)=>{
        const fetchingData = () => {
            setLoading(true);
            getRechargeOrder().then((response) => {
                setLoading(false);
                const orders = response.data.map(order => {
                    return {
                        ...order,
                        key: order.orderId
                    }
                });
                setResult(orders.filter(order =>
                    (condition.orderId ? order.orderId.includes(condition.orderId) : true) &&
                    (condition.status ? (order.status + "") === (condition.status) : true) &&
                    (condition.transactionId ? order.transactionId.includes(condition.transactionId) : true) &&
                    (condition.start ? condition.start.isSameOrBefore(order.createdDate, "day") : true) &&
                    (condition.end ? condition.end.isSameOrAfter(order.createdDate, "day") : true)
                ));
            });
            setLoading(false);
        };
        fetchingData();
    },[]);
    
    const showModal = (order) => {
        setOrder(order);
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const filterOrderIdChange = (e) => {
        let value = e.target.value;
        setCondition({
            ...condition,
            orderId: value
        });
    };
    const filterStatusChange = (value) => {
        setCondition({
            ...condition,
            status: value
        });
    };
    const filterTxIdChange = (e) => {
        let value = e.target.value;
        setCondition({
            ...condition,
            transactionId: value
        })
    };
    const filterDateRangeChange = (value, dateString) => {
        setCondition({
            ...condition,
            start: value ? value[0] : null,
            end: value ? value[1] : null
        })
    };

    const update = () => {
        message.success(t("callbackSuccess"))
        fetchData(condition);
    };

    useEffect(() => {
        fetchData(condition);
    },[fetchData,condition]);

    useEffect(() => {
        const id = setInterval(()=>{
            fetchData(condition);
        }, 1000);
        return () => clearInterval(id);
    }, [fetchData,condition]);

    return (
        <div className="panel">
            <Card title={t("rechargeOrder")} bordered={true} extra={`${t("lastUpdatedDate")} ${new Date().toLocaleString()}`}>
                <Form layout="inline">
                    <Form.Item label={`${t("order.orderId")}:`}>
                        <Input onChange={filterOrderIdChange} />
                    </Form.Item>
                    <Form.Item label="TxId:">
                        <Input onChange={filterTxIdChange} />
                    </Form.Item>
                    <Form.Item label={`${t("order.status")}:`} >
                        <Select
                            style={{ width: 120 }}
                            onChange={filterStatusChange}>
                            <Select.Option value="">ALL</Select.Option>
                            <Select.Option value="1">{t("status.process")}</Select.Option>
                            <Select.Option value="2">{t("status.success")}</Select.Option>
                            <Select.Option value="-1">{t("status.fail")}</Select.Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label={`${t("order.createdDate")}:`}>
                        <RangePicker
                            onChange={filterDateRangeChange}
                        />
                    </Form.Item>
                </Form>
                <br />
                <Table
                    bordered
                    size="small"
                    columns={columns}
                    dataSource={result}
                    loading={loading}
                />
            </Card>
            <Modal title={t("detailInfo")} visible={isModalVisible} onCancel={handleCancel} width={700} footer={null}>
                <Descriptions
                    size="small"
                    bordered
                    column={{ xxl: 2, xl: 2, lg: 2, md: 2, sm: 2, xs: 2 }}
                >
                    <Descriptions.Item label={t("order.orderId")} span={2}>{order.orderId}</Descriptions.Item>
                    <Descriptions.Item label={t("user.account")}>{props.account}</Descriptions.Item>
                    <Descriptions.Item label={t("order.chain")}>{order.chain}</Descriptions.Item>
                    <Descriptions.Item label={t("order.amount")}>{order.amount}</Descriptions.Item>
                    <Descriptions.Item label={t("order.handlingFee")}>{order.handlingFee}</Descriptions.Item>
                    <Descriptions.Item label={t("order.createdDate")}>{new Date(order.createdDate).toLocaleString()}</Descriptions.Item>
                    <Descriptions.Item label={t("order.callbackDate")}>{new Date(order.callbackDate).toLocaleString()}</Descriptions.Item>
                    <Descriptions.Item label={t("order.callbackCounter")}>{order.callbackCounter}</Descriptions.Item>
                    <Descriptions.Item label={t("order.status")}><StatusTag status={order.status} /></Descriptions.Item>
                    <Descriptions.Item label="TxID" span={2}>{order.transactionId}</Descriptions.Item>
                    <Descriptions.Item label={t("order.callbackUrl")} span={2}>{order.callbackUrl}</Descriptions.Item>
                    <Descriptions.Item label={t("order.callbackResponse")} span={2}>{order.callbackResponse}</Descriptions.Item>
                </Descriptions>
            </Modal>
        </div>
    );
}

export default connect((state) => state.user)(RechargeOrder);