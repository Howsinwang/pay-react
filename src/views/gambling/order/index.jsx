import React, { useEffect, useState } from "react";
import { connect } from "react-redux"
import { Table, Card, Modal, Descriptions, message } from "antd";
import OperationButton from "@/components/OperationButton";
import StatusTag from "@/components/StatusTag";
import { getGamblingOrder } from "@/api/order";
import "./index.less"
import { useTranslation } from 'react-i18next';

const GamblingOrder = (props) => {
    const { t } = useTranslation();
    const columns = [
        {
            title: t("gamblingOrder.orderId"),
            dataIndex: 'orderId',
            render: orderId => `${orderId.substring(0, 15)}...`,
            width: '15%',
            key: 'orderId',
        },
        {
            title: t("gamblingOrder.amount"),
            dataIndex: 'amount',
            key: 'amount',
        },
        {
            title: t("gamblingOrder.blockNumber"),
            dataIndex: 'blockNumber',
            key: 'blockNumber',
        },
        {
            title: t("gamblingOrder.status"),
            dataIndex: 'status',
            key: 'status',
            render: (status) => <StatusTag status={status}/>
        },
        {
            title: t("gamblingOrder.createdDate"),
            dataIndex: 'createdDate',
            key: 'createdDate',
            render: createdDate => new Date(createdDate).toLocaleString(),
            defaultSortOrder: 'descend',
            sorter: (a, b) => (
                new Date(a.createdDate).getTime() - new Date(b.createdDate).getTime()
            )
        },
        {
            title: t("operation"),
            render: (order) => (
                <OperationButton 
                    order={order} 
                    operation={{
                        detail : showModal,
                        callback : update
                    }}
                />
            )
        }
    ];
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [loading, setLoading] = useState(false);
    const [order, setOrder] = useState({});
    const [result, setResult] = useState([]);

    const getData = async () => {
        setLoading(true);
        try {
            const orderArray = await getGamblingOrder();
            setResult(orderArray.data.map(position => ({ ...position, key: position.positionId })));
            setLoading(false);
        } catch (err) {
            // console.log(err)
        }
    }
    
    const showModal = (order) => {
        setOrder(order);
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const update = () => {
        message.success(t("callbackSuccess"))
        getData()
    };

    useEffect(() => {
        getData()
    }, [])

    return (
        <div className="panel">
            <Card title={t("menu.gamblingOrder")} bordered={true} extra={`${t("lastUpdatedDate")} ${new Date().toLocaleString()}`}>
                <Table
                    bordered
                    size="small"
                    columns={columns}
                    dataSource={result}
                    loading={loading}
                />
            </Card>
            <Modal title={t("detailInfo")} visible={isModalVisible} onCancel={handleCancel} width={700} footer={null}>
                <Descriptions
                    size="small"
                    bordered
                    column={{ xxl: 2, xl: 2, lg: 2, md: 2, sm: 2, xs: 2 }}
                >
                    <Descriptions.Item label={t("gamblingOrder.orderId")} span={2}>{order.orderId}</Descriptions.Item>
                    <Descriptions.Item label={t("user.account")} span={1}>{props.account}</Descriptions.Item>
                    <Descriptions.Item label={t("gamblingOrder.chain")} span={1}>Tron</Descriptions.Item>
                    <Descriptions.Item label={t("gamblingOrder.amount")} span={1}>{order.amount}</Descriptions.Item>
                    <Descriptions.Item label={t("gamblingOrder.handlingFee")} span={1}>{order.handlingFee}</Descriptions.Item>
                    <Descriptions.Item label={t("gamblingOrder.createdDate")} span={1}>{new Date(order.createdDate).toLocaleString()}</Descriptions.Item>
                    <Descriptions.Item label={t("gamblingOrder.status")} span={1}><StatusTag status={order.status} /></Descriptions.Item>
                    <Descriptions.Item label="TxID" span={2}>{order.transactionId}</Descriptions.Item>
                    <Descriptions.Item label={t("gamblingOrder.blockNumber")} span={1}>{order.blockNumber}</Descriptions.Item>
                    <Descriptions.Item label={t("gamblingOrder.blockTimestamp")} span={1}>{order.blockTimestamp}</Descriptions.Item>
                    <Descriptions.Item label={t("gamblingOrder.blockHash")} span={2}>{order.blockHash}</Descriptions.Item>
                    <Descriptions.Item label={t("gamblingOrder.callbackDate")} span={1}>{new Date(order.callbackDate).toLocaleString()}</Descriptions.Item>
                    <Descriptions.Item label={t("gamblingOrder.callbackCounter")} span={1}>{order.callbackCounter}</Descriptions.Item>
                    <Descriptions.Item label={t("gamblingOrder.callbackUrl")} span={2}>{order.callbackUrl}</Descriptions.Item>
                    <Descriptions.Item label={t("gamblingOrder.callbackResponse")} span={2}>{order.callbackResponse}</Descriptions.Item>
                </Descriptions>
            </Modal>
        </div>
    );
}

export default connect((state) => state.user)(GamblingOrder);