import React, { useEffect, useState } from "react";
import { connect } from "react-redux"
import { Table, Button, Card, Modal, Input, Form, message } from "antd";
import { getWallets, createWallet, deleteWallet } from "@/api/wallet";
import { useTranslation } from 'react-i18next';

const { confirm } = Modal;
const SysUser = () => {

    // ? init i18n
    const { t } = useTranslation();

    // ? state
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [isModalVisible, setIsModalVisible] = useState(false);

    const columns = [
        {
            title: t("walletId"),
            dataIndex: 'walletId',
            key: 'walletId',
        },
        {
            title: t("walletAddress"),
            dataIndex: 'walletAddress',
        },
        {
            title: t("operation"),
            render: (wallet) => (
              <Button 
                type="danger" 
                size="small"
                onClick={() => deleteGamblingWallet(wallet)} 
              >{t("button.delete")}</Button>
            )
        }
    ];

    const getData = async () => {
        setLoading(true);
        try {
            const walletArray = await getWallets("gambling");
            setData(walletArray.data.data.map(wallet => ({ ...wallet, key: wallet.walletId })));
            setLoading(false);
        } catch (err) {
            // console.log(err)
        }
    }

    useEffect(() => {
        getData()
    }, [])

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleOk = (value) => {
        createWallet("gambling",value).then((response)=>{
          getData();
          message.success(response.data.msg);
        }).catch((error) => {
          message.error(error.response.data.msg);
      });
        setIsModalVisible(false);
    };

    const deleteGamblingWallet = (wallet) => {
      confirm({
        title: t("title.deleteWallet"),
        content: `${t("notification.confirmDeleteWallet")} ${wallet.walletAddress}`,
        onOk: () => {
          let param = {
              "walletId": wallet.walletId
          };
          deleteWallet("gambling",param)
            .then((response) => {
                message.success(response.data.msg);
                getData()
            })
            .catch((error)=>{
                if ( error.response.status===400 ){
                    message.error(error.response.data.msg);
                }
            });
        },
        onCancel: () => {}
      })
    }

    return (
        <div className="panel">
            <br />
            <Card title={t("menu.gamblingWallet")} bordered={true}>
                <Button
                    type="primary"
                    style={{
                        marginBottom: 10
                    }}
                    onClick={()=>showModal()}
                >{t("button.addWallet")}</Button>
                <Table
                    bordered
                    size="small"
                    columns={columns}
                    dataSource={data}
                    loading={loading}
                />
            </Card>
            {
              isModalVisible &&
              <Modal
                  title={t("title.addWallet")}
                  visible={isModalVisible}
                  onCancel={handleCancel}
                  width={600}
                  footer={null}
              >
                  <Form
                    onFinish={handleOk}
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 16 }}
                >
                    <Form.Item
                        label={t("walletAddress")}
                        name="address"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("notification.needWallet"),
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{ offset: 18 }}
                    >
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="login-form-button"
                        >
                            {t("add")}
                        </Button>
                    </Form.Item>
                </Form>
              </Modal>
            }
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
      ...state,
      user: state.user,
    };
};

export default connect(mapStateToProps)(SysUser);