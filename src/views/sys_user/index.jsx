import React from "react";
import "./index.less";
import PanelGroup from "./PanelGroup";

const Dashboard = () => {
  return (
    <div className="app-container">
      <PanelGroup/>
    </div>
  );
};

export default Dashboard;
