import React, { useEffect, useState } from "react";
import { Row, Col } from "antd";
import "./index.less";
import { getInfo } from "@/api/info";
import { useTranslation } from 'react-i18next';

const PanelGroup = (props) => {
  const { t } = useTranslation();
  const [chartList, setChartList] = useState([]);
  useEffect(() => {
    getInfo().then((resp) => {
      const info = resp.data;
      if ( chartList.length===0 ){
        var block = [
          ...chartList,
          {
            type: t("todayRecharge"),
            num: info.todayRecharge,
            color: "#40c9c6",
          },
          {
            type: t("yesterdayRecharge"),
            num: info.yesterdayRecharge,
            color: "#40c9c6",
          },
          {
            type: t("todayDeposit"),
            num: info.todayDeposit,
            color: "#40c9c6",
          },
          {
            type: t("yesterdayDeposit"),
            num: info.yesterdayDeposit,
            color: "#40c9c6",
          },
          {
            type: t("todayWithdraw"),
            num: info.todayWithdraw,
            color: "#40c9c6",
          },
          {
            type: t("yesterdayWithdraw"),
            num: info.yesterdayWithdraw,
            color: "#40c9c6",
          },
          {
            type: t("todayTax"),
            num: info.todayTax,
            color: "#40c9c6",
          },
          {
            type: t("yesterdayTax"),
            num: info.yesterdayTax,
            color: "#40c9c6",
          },
          {
            type: t("todaySystemFee"),
            num: info.todaySystemFee,
            color: "#40c9c6",
          },
          {
            type: t("yesterdaySystemFee"),
            num: info.yesterdaySystemFee,
            color: "#40c9c6",
          },
        ];
        setChartList(block);
      }

    });
  });
  return (
    <div className="panel-group-container">
      <Row gutter={40} className="panel-group">
        {chartList.map((chart, i) => (
          <Col
            key={i}
            lg={6}
            sm={12}
            xs={12}
            className="card-panel-col"
          >
            <div className="card-panel">
              <div className="card-panel-description">
                <p className="card-panel-text">{chart.type}</p>
                <h2>{chart.num}</h2>
              </div>
            </div>
          </Col>
        ))}
      </Row>
    </div>
  );
};

export default PanelGroup;
