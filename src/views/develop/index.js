import React, { useState, useEffect } from 'react';
import { Client } from '@stomp/stompjs';
import SockJS from 'sockjs-client';
import { Button, notification } from "antd";

const SOCKET_URL = 'http://localhost:5050/ws';

const App = () => {
    const [message, setMessage] = useState('You server message here.');
    const [isSocketConnect, setIsSocketConnect] = useState(false);
    const [socketClient, setSocketClient] = useState(false);
    const [currentChannel,setCurrentChannel] = useState("");

    const openNotificationWithIcon = (type, title, message) => {
        notification[type]({
            message: title,
            description: message,
        });
    };

    const getAddressAndNotices = async () => {
        try {
            subscribeChannel("/topic/message")
        } catch (err) {
            console.log(err);
        }
    }

    const subscribeChannel = (address) => {

        if (socketClient === null) return
        socketClient.subscribe(address, function (responseBody) {
            const data = JSON.parse(responseBody.body)
            setMessage(data.msg);
            openNotificationWithIcon("success","Channel All",data.msg);
        })
    }

    function socketConnect() {
        const client = new Client({
            debug: () => { },
            reconnectDelay: 5000,
            heartbeatIncoming: 4000,
            heartbeatOutgoing: 4000,
            connectionTimeout: 3000
        });

        setSocketClient(client);

        client.webSocketFactory = function () {
            return new SockJS(SOCKET_URL+'?token=eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NTYwNTk3NzQsImFjY291bnQiOiJkZW1vMTIzNDU2In0.w4LggucaJQ0CfprfJhYuKk3vc8rG9HxTnW1vdKQtbpM' );
        };

        client.beforeConnect = () => { }

        client.onConnect = (state) => {
            if (state.command === 'CONNECTED') {
                setIsSocketConnect(true)
            }
        }

        client.onStompError = (frame) => {
            if (frame?.headers?.message?.endsWith('Invalid token.')) {
                client.deactivate()
            }
        }

        client.onWebSocketClose = (frame) => {
            if (frame.type === 'close') {
                setIsSocketConnect(false)
            }
        }

        client.activate()
    }
    const subscribe = (channel) => {
        setCurrentChannel(channel);
        if (socketClient === null) return
        socketClient.subscribe(channel, function (responseBody) {
            const data = JSON.parse(responseBody.body)
            setMessage(data.msg);
            // openNotificationWithIcon("info",channel,data.msg);
        })
    }

    useEffect(() => {
        socketConnect()
    }, [])

    useEffect(() => {
        if (isSocketConnect) {
            getAddressAndNotices()
        } else {
            console.log("disconnect");
        }
    }, [isSocketConnect])

    return (
        <div>
            <h1>{isSocketConnect ? "Connect!" : "Disconnect!"}</h1>
            <h1>{currentChannel}</h1>
            <h1>{message}</h1>
            <Button onClick={()=>subscribe("/topic/channel/A")}>subscribe channel A</Button>
            <Button onClick={()=>subscribe("/topic/channel/B")}>subscribe channel B</Button>
        </div>
    );
}

export default App;