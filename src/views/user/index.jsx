import React, { useEffect, useState, useCallback } from "react";
import { connect } from "react-redux"
import { Table, Button, Card, Modal, Form, Input, message } from "antd";
import { getUsers, addUser } from "@/api/user";
import { useTranslation } from 'react-i18next';
import "./index.less"

const SysUser = (props) => {
    const { user } = props;
    const { t } = useTranslation();
    const [columns,setColumns] = useState([
        {
            title: t("user.account"),
            dataIndex: 'account',
            key: 'account',
        },
        {
            title: t("user.name"),
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: t("user.role"),
            dataIndex: 'roles',
            key: 'roles',
            render: (roles) => {
                let rolesStr = "";
                roles.forEach(role => {
                    switch (role) {
                        case 1: rolesStr += t("admin"); break;
                        case 2: rolesStr += t("partner"); break;
                        case 3: rolesStr += t("viewer"); break;
                        default: break;
                    }
                });
                return rolesStr;
            }
        },
        {
            title: t("partner.partnerId"),
            dataIndex: 'partner',
            key: 'partner',
            render: (partner) => {
                return partner===null ? "" : partner.partnerId;
            }
        },
        {
            title: t("balance"),
            dataIndex: 'partner',
            key: 'partner',
            render: (partner) => {
                return partner===null ? "" : partner.usdt;
            }
        },
        {
            title: t("remark"),
            dataIndex: 'remark',
            key: 'remark',
        },
        {
            title: t("user.createdDate"),
            dataIndex: 'createdDate',
            key: 'createdDate',
            render: createdDate => new Date(createdDate).toLocaleString(),
            defaultSortOrder: 'descend',
            sorter: (a, b) => (
                new Date(a.createdDate).getTime() - new Date(b.createdDate).getTime()
            )
        },
        {
            title: t("operation")
        }
    ]);

    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(false);
    const [isModalVisible, setIsModalVisible] = useState(false);

    const fetchData = useCallback(() => {
        const fetchingData = () => {
            setLoading(true);
            if (data.length === 0) {
                getUsers().then((response) => {
                    setLoading(false);
                    const users = response.data.map(user => {
                        return {
                            ...user,
                            key: user.userId
                        }
                    });
                    setData(users);
                });
            }
            setLoading(false);
        };
        fetchingData();
    }, [data]);

    const showModal = () => {
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const handleOk = (values) => {
        addUser(values).then((response) => {
            console.log(response);
            const newUser = response.data.data;
            setData([
                ...data,
                {
                    key:newUser.userId,
                    ...newUser
                }
            ])
            message.success(response.data.msg);
        });
        setIsModalVisible(false);
    };

    useEffect(() => {
        if ( user.roles.indexOf(1)===-1 ){
            setColumns(columns.filter(column=>column.dataIndex!=="partner"));
        }
    },[]);

    useEffect(() => {
        fetchData();
    },[data,fetchData]);

    return (
        <div className="panel">
            <br />
            <Card title={t("menu.userManage")} bordered={true}>
                <Form layout="inline">
                    <Button type="primary" size="small" className="btn-opt" onClick={() => showModal()}>{t("addUser")}</Button>
                </Form>
                <br />
                <Table
                    bordered
                    size="small"
                    columns={columns}
                    dataSource={data}
                    loading={loading}
                />
            </Card>
            <Modal
                title={t("addUser")}
                visible={isModalVisible}
                onCancel={handleCancel}
                width={400}
                footer={null}
            >
                <Form
                    onFinish={handleOk}
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 16 }}
                >
                    <Form.Item
                        label={t("user.name")}
                        name="name"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("notice.name"),
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label={t("user.account")}
                        name="account"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("notice.account"),
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label={t("password")}
                        name="password"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("notice.password"),
                            },
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item
                        label={t("typeAgain")}
                        name="password2"
                        dependencies={['password']}
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("notice.password"),
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('password') === value) {
                                        return Promise.resolve();
                                    }
                                    return Promise.reject(new Error(t("passwordDiff")));
                                },
                            })
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    {/* <Form.Item name="role" label="權限" rules={[{ required: true }]}>
                        <Select>
                            <Option value="1">admin</Option>
                            <Option value="2">partner</Option>
                        </Select>
                    </Form.Item> */}
                    <Form.Item
                        label={t("remark")}
                        name="remark"
                    >
                        <Input.TextArea rows={4} />
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{ offset: 18 }}
                    >
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="login-form-button"
                        >
                            {t("add")}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
      ...state,
      user: state.user,
    };
};
  
export default connect(mapStateToProps)(SysUser);