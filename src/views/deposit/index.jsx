import React, { useEffect, useState, useCallback } from "react";
import { connect } from "react-redux"
import { Table, Card, Modal, Descriptions, DatePicker, Form, Input, Select, message, Button } from "antd";
import StatusTag from "@/components/StatusTag";
import OperationButton from "@/components/OperationButton";
import { getDepositOrder } from "@/api/order";
import "./index.less"
import ReplenishModal from './replenish';
import ReverseModal from './reverse';
import { useTranslation } from 'react-i18next';

const { RangePicker } = DatePicker;
const DepositOrder = (props) => {
    const { t } = useTranslation();
    const columns = [
        {
            title: t('order.orderId'),
            dataIndex: 'orderId',
            render: orderId => `${orderId.substring(0, 15)}...`,
            width: '15%',
            key: 'orderId',
        },
        {
            title: t("order.userOrderId"),
            dataIndex: 'userOrderId',
            render: userOrderId => `${userOrderId.substring(0, 15)}...`,
            width: '15%',
            key: 'userOrderId',
        },
        {
            title: t("order.currencySymbol"),
            dataIndex: 'currencySymbol',
            key: 'currencySymbol',
            render: currencySymbol => currencySymbol==null ? "USDT" : currencySymbol.toUpperCase()
        },
        {
            title: t("order.currencyAmount"),
            key: 'currencyAmount',
            render: order => getOrderAmount(order)
        },
        {
            title: t("order.receiveAmount"),
            dataIndex: 'receiveAmount',
            key: 'receiveAmount',
        },
        {
            title: t("order.chain"),
            dataIndex: 'chain',
            key: 'chain',
        },
        {
            title: t("order.handlingFee"),
            dataIndex: 'handlingFee',
            key: 'handlingFee',
        },
        {
            title: t("order.status"),
            dataIndex: 'status',
            key: 'status',
            render: (status) => <StatusTag status={status}/>
        },
        {
            title: t("order.createdDate"),
            dataIndex: 'createdDate',
            key: 'createdDate',
            render: createdDate => new Date(createdDate).toLocaleString(),
            defaultSortOrder: 'descend',
            sorter: (a, b) => (
                new Date(a.createdDate).getTime() - new Date(b.createdDate).getTime()
            )
        },
        {
            title: t("operation"),
            render: (order) => (
                <OperationButton 
                    order={order} 
                    operation={{
                        detail: showModal,
                        callback: update,
                        replenish: replenish,
                        reverse: reverse
                    }}
                />
            )
        }
    ];
    const [isModalVisible, setIsModalVisible] = useState("");
    const [condition, setCondition] = useState({});
    const [loading, setLoading] = useState(false);
    const [order, setOrder] = useState({});
    const [result, setResult] = useState([]);

    const fetchData = useCallback((condition)=>{
        const fetchingData = () => {
            setLoading(true);
            getDepositOrder().then((response) => {
                setLoading(false);
                const orders = response.data.map(order => {
                    return {
                        ...order,
                        key: order.orderId
                    }
                });
                setResult(orders.filter(order =>
                    (condition.orderId ? order.orderId.includes(condition.orderId) : true) &&
                    (condition.userOrderId ? order.userOrderId.includes(condition.userOrderId) : true) &&
                    (condition.status ? (order.status + "") === (condition.status) : true) &&
                    (condition.transactionId ? order.transactionId.includes(condition.transactionId) : true) &&
                    (condition.start ? condition.start.isSameOrBefore(order.createdDate, "day") : true) &&
                    (condition.end ? condition.end.isSameOrAfter(order.createdDate, "day") : true)
                ));
            });
            setLoading(false);
        };
        fetchingData();
    },[]);
    
    const getOrderAmount = (order) => {
        return order.currencyAmount==null ? order.amount : order.currencyAmount;
    };

    const showModal = (order) => {
        setOrder(order);
        setIsModalVisible("detail");
    };

    const replenish = (order) => {
        setOrder(order);
        setIsModalVisible("replenish");
    };

    const handleCancel = () => {
        setIsModalVisible("");
    };

    const filterOrderIdChange = (e) => {
        let value = e.target.value;
        setCondition({
            ...condition,
            orderId: value
        });
    };
    const filterStatusChange = (value) => {
        setCondition({
            ...condition,
            status: value
        });
    };
    const filterTxIdChange = (e) => {
        let value = e.target.value;
        setCondition({
            ...condition,
            transactionId: value
        })
    };
    const filterDateRangeChange = (value, dateString) => {
        setCondition({
            ...condition,
            start: value ? value[0] : null,
            end: value ? value[1] : null
        })
    };
    const filterUserOrderIdChange = (e) => {
        let value = e.target.value;
        setCondition({
            ...condition,
            userOrderId: value
        });
    };

    const update = () => {
        message.success(t("callbackSuccess"))
        fetchData(condition);
    };

    const showTransactionDetail = (transaction) => {
        setIsModalVisible("");
        message.success(t("callbackSuccess"))
        fetchData(condition);
    };

    const showResultMessage = (msg) => {
        setIsModalVisible("");
        message.success(msg)
        fetchData(condition);
    };

    const reverse = (order) => {
        setOrder(order);
        setIsModalVisible("reverse");
    };

    const handleDownload = (type) => {
        import("@/lib/Export2Excel").then((excel) => {
            const tHeader = [
                t("order.orderId"),
                t("order.userOrderId"),
                t("order.chain"),
                t("order.currencyAmount"),
                t("order.amount")+"(USDT)",
                t("order.receiveAmount"),
                t("order.receiveAmount")+"(USDT)",
                t("rate"),
                t("order.handlingFee"),
                t("order.createdDate"),
                t("order.status"),
                "TxID",
                t("receiveWallet")
            ];
            const filterVal = [
                "orderId",
                "userOrderId",
                "chain",
                "currencyAmount",
                "amount",
                "currencyReceiveAmount",
                "receiveAmount",
                "rate",
                "handlingFee",
                "createdDate",
                "status",
                "transactionId",
                "receiveWallet"
            ];
            const data = formatJson(filterVal, result.map(record=>{
                record.createdDate = new Date(record.createdDate).toLocaleString();
                record.currencyReceiveAmount = record.receiveAmount*record.exchangeRate;
                
                switch (record.status) {
                    case 0: record.status = t("status.new"); break;
                    case 1: record.status = t("status.process"); break;
                    case 2: record.status = t("status.success"); break;
                    case -1: record.status = t("status.fail"); break;
                    case -2: record.status = t("status.timeout"); break;
                    case -3: record.status = t("status.cancel"); break;
                    default: record.status = "";
                }
                return record;
            }));
            
            excel.export_json_to_excel({
                header: tHeader,
                data,
                filename: `${props.account}-${Date.now()}`,
                autoWidth: true,
                bookType: "xlsx",
            });
        });
    };

    const formatJson = (filterVal, jsonData) => {
        return jsonData.map(v => filterVal.map(j => v[j]))
    }

    useEffect(() => {
        fetchData(condition);
    },[fetchData,condition]);

    useEffect(() => {
        const id = setInterval(()=>{
            fetchData(condition);
        }, 1000);
        return () => clearInterval(id);
    }, [fetchData,condition]);

    return (
        <div className="panel">
            <Card title={t("depositOrder")} bordered={true} extra={`${t("lastUpdatedDate")} ${new Date().toLocaleString()}`}>
                <Form 
                    layout="inline"
                    >
                    <Form.Item label={`${t("order.orderId")}:`}>
                        <Input onChange={filterOrderIdChange} />
                    </Form.Item>
                    <Form.Item label={`${t("order.userOrderId")}:`}>
                        <Input onChange={filterUserOrderIdChange} />
                    </Form.Item>
                    <Form.Item label="TxId:">
                        <Input onChange={filterTxIdChange} />
                    </Form.Item>
                    <Form.Item wrapperCol={{ span: 2 }} label={`${t("order.status")}:`} >
                        <Select
                            style={{ width: 120 }}
                            onChange={filterStatusChange}>
                            <Select.Option value="">ALL</Select.Option>
                            <Select.Option value="0">{t("status.new")}</Select.Option>
                            <Select.Option value="1">{t("status.process")}</Select.Option>
                            <Select.Option value="2">{t("status.success")}</Select.Option>
                            <Select.Option value="-1">{t("status.fail")}</Select.Option>
                            <Select.Option value="-2">{t("status.timeout")}</Select.Option>
                            <Select.Option value="-3">{t("status.cancel")}</Select.Option>
                        </Select>
                    </Form.Item>
                    <Form.Item label={`${t("order.createdDate")}:`}>
                        <RangePicker
                            onChange={filterDateRangeChange}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button onClick={handleDownload}>{t("export")}</Button>
                    </Form.Item>
                </Form>
                <br />
                <Table
                    bordered
                    size="small"
                    columns={columns}
                    dataSource={result}
                    loading={loading}
                />
            </Card>
            <Modal title={t("detailInfo")} visible={isModalVisible==="detail"} onCancel={handleCancel} width={700} footer={null}>
                <Descriptions
                    size="small"
                    bordered
                    column={{ xxl: 2, xl: 2, lg: 2, md: 2, sm: 2, xs: 2 }}
                >
                    <Descriptions.Item label={t("order.orderId")} span={2}>{order.orderId}</Descriptions.Item>
                    <Descriptions.Item label={t("order.userOrderId")} span={2}>{order.userOrderId}</Descriptions.Item>
                    <Descriptions.Item label={t("user.account")}>{props.account}</Descriptions.Item>
                    <Descriptions.Item label={t("order.chain")}>{order.chain}</Descriptions.Item>
                    <Descriptions.Item label={`${t("order.currencyAmount")}(${order.currencySymbol})`}>{order.currencyAmount}</Descriptions.Item>
                    <Descriptions.Item label={`${t("order.amount")}(USDT)`}>{order.amount}</Descriptions.Item>
                    <Descriptions.Item label={`${t("order.receiveAmount")}(${order.currencySymbol})`}>{order.receiveAmount*order.exchangeRate}</Descriptions.Item>
                    <Descriptions.Item label={`${t("order.receiveAmount")}(USDT)`}>{order.receiveAmount}</Descriptions.Item>
                    <Descriptions.Item label={t("rate")}>{order.exchangeRate}</Descriptions.Item>
                    <Descriptions.Item label={t("order.handlingFee")}>{order.handlingFee}</Descriptions.Item>
                    <Descriptions.Item label={t("order.createdDate")}>{new Date(order.createdDate).toLocaleString()}</Descriptions.Item>
                    <Descriptions.Item label={t("order.callbackDate")}>{new Date(order.callbackDate).toLocaleString()}</Descriptions.Item>
                    <Descriptions.Item label={t("order.callbackCounter")}>{order.callbackCounter}</Descriptions.Item>
                    <Descriptions.Item label={t("order.status")}><StatusTag status={order.status}/></Descriptions.Item>
                    <Descriptions.Item label="TxID" span={2}>{order.transactionId}</Descriptions.Item>
                    <Descriptions.Item label={t("receiveWallet")} span={2}>{order.receiveWallet}</Descriptions.Item>
                    <Descriptions.Item label={t("order.callbackUrl")} span={2}>{order.callbackUrl}</Descriptions.Item>
                    <Descriptions.Item label={t("order.callbackResponse")} span={2}>{order.callbackResponse}</Descriptions.Item>
                </Descriptions>
            </Modal>
            <ReplenishModal 
                visible={isModalVisible==="replenish"}
                onCancel={handleCancel}
                onFinish={showTransactionDetail}
                orderId={order.orderId}
                t={t}
            />
            <ReverseModal 
                visible={isModalVisible==="reverse"}
                onCancel={handleCancel}
                onFinish={showResultMessage}
                orderId={order.orderId}
                t={t}
            />
        </div>
    );
}

export default connect((state) => state.user)(DepositOrder);