import React from 'react';
import { Button, Modal, Form, Input, message } from "antd";
import { replenishOrder } from "@/api/order";

const ReplenishModal = (props) => {
    const {visible,onCancel,onFinish,orderId,t } = props;
    const handleOk = (values) => {
        console.log(orderId);
        var param = {
            ...values,
            orderId: orderId
        };

        replenishOrder(param).then((response)=>{
            const { msg } = response.data;
            onFinish(msg);
        }).catch((error) => {
            console.log(error);
            message.error(error.response.data.msg);
        });
    };
    return (
        <div>
            <Modal
                title={t("replenishOrder")}
                visible={visible}
                onCancel={onCancel}
                width={400}
                footer={null}
            >
                <Form
                    onFinish={handleOk}
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 16 }}
                >
                    <Form.Item
                        label="TxID"
                        name="transactionId"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("noticeTransactionId"),
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label={t("verifyCode")}
                        name="code"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("noticeVerifyCode"),
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{ offset: 18 }}
                    >
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="login-form-button"
                        >
                            {t("confirm")}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}

export default ReplenishModal;