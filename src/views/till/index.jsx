import React,{ useState, useEffect, useCallback } from 'react';
import { Redirect,useLocation } from "react-router-dom";
import logo from "@/assets/images/logo.svg";
import "./index.less"
import { Card, List, Skeleton,Button, Row, Col, Statistic, Divider, Modal, message } from "antd";
import QRCode from 'qrcode.react';
import CNY from 'cryptocurrency-icons/svg/color/cny.svg';
import USDT from 'cryptocurrency-icons/svg/color/usdt.svg';
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons';
import { getDeposit,confirmPay,cancelOrder } from '@/api/order';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { useTranslation } from 'react-i18next';

const { Countdown } = Statistic;

const Till = () => {
    const { t, i18n } = useTranslation();
    const useQuery = () => {
        return new URLSearchParams(useLocation().search);
    }

    const [detail,setDetail] = useState([]);
    const [redirect,setRedirect] = useState(false);
    const [deadline,setDeadline] = useState(0);
    const [query] = useState(useQuery());
    const [order,setOrder] = useState({});
    const [mode,setMode] = useState("rate");
    const [display,setDisplay] = useState("none");

    const [params] = useState({
        orderId: query.get("orderId"),
        lang: query.get("lang"),
    });

    const onCopy = () => {
        message.success(t("walletCopySuccess"));
    };

    const changeLanguage = useCallback((lang) => {
        const changeLanguageHandler = () =>
        {
            i18n.changeLanguage(lang)
        }
        changeLanguageHandler();
    },[i18n]);
    

    useEffect(()=>{
        if ( params.lang!=null ){
            changeLanguage(params.lang);
        }
        setDisplay("block");
    },[changeLanguage,params]);

    useEffect(()=>{
        getDeposit(params).then((resp)=>{
            var order = resp.data.data;
            if ( order===null ){
                setRedirect(true);
            }
            else {
                console.log(order);
                setMode(order.exchangeRate==null ? "usdt" : "rate");

                setDetail([
                    {
                        key: "orderId",
                        title: t("order.orderId"),
                        value: order.orderId
                    },
                    {
                        key: "walletAddress",
                        title: t("walletAddress"),
                        value: order.receiveWallet,
                        extra:(
                            <CopyToClipboard onCopy={onCopy} text={order.receiveWallet}>
                                <Button type="primary" size="small">{t("copy")}</Button>
                            </CopyToClipboard>
                        )
                    },
                    {
                        key: "createdDate",
                        title: t("order.createdDate"),
                        extra: <div className="value">{new Date(order.createdDate).toLocaleString()}</div>,
                    },
                    {
                        key: "rate",
                        title: `${t("rate")} (CYN/USDT)`,
                        extra: <div className="value">{order.exchangeRate}</div>,
                    },
                ]);
                setOrder(order);
                setDeadline(new Date(order.createdDate).getTime() + 1000 * 60 * 10);
            }
        });
    },[redirect]);

    if ( redirect || (deadline!==0&&deadline-Date.now()<=0) ){
        return <Redirect to={`/error/timeout/${params.lang==null? "zh-CN" : params.lang}`} />;
    }

    const confirm = () => {
        Modal.confirm({
            title: t("confirmPay"),
            content:
              t("confirmPayMessage"),
            okText: t("confirm"),
            cancelText: t("cancel"),
            onOk() {
                confirmPay({orderId:order.orderId}).then((resp)=>{
                    message.success(resp.data.msg);
                    setRedirect(true);
                });
            },
            onCancel() {
              console.log("Cancel");
            },
        });
        
    }

    const cancel = () => {
        Modal.confirm({
            title: t("confirmCancelOrder"),
            content:
              t("confirmCancelOrderMessage"),
            okText: t("confirm"),
            cancelText: t("cancel"),
            onOk() {
                cancelOrder({orderId:order.orderId}).then((resp)=>{
                    message.success(resp.data.msg);
                    setRedirect(true);
                });
            },
            onCancel() {
              console.log("Cancel");
            },
        });
    }

    const getListItem = (item) => {
        return (
            <List.Item
                actions={[item.extra]}
                className="items"
            >
                <Skeleton title={false} loading={false} active>
                    <List.Item.Meta
                        title={item.title}
                        description={<div className="value">{item.value} </div>}
                    />
                </Skeleton>
            </List.Item>
        );
    }

    return (
        <div className="container">
            <Card
                style={{display:display}}
                title={
                    <div className="title">
                        <img src={logo} className="logo" alt="logo"/>{t("platformName")}
                    </div>
                }
                extra={<Countdown title={t("timeLeft")} value={deadline} onFinish={()=>setRedirect(true)} />}
                className="card"
                actions={[
                    <div style={{color:"red"}} onClick={cancel}><CloseCircleOutlined className="icon"/>{t("cancelOrder")}</div>,
                    <div style={{color:"green"}} onClick={confirm}><CheckCircleOutlined className="icon"/>{t("alreadyPay")}</div>,
                ]}
            >
                <Row gutter={8} style={mode!=="usdt" ? {display:"none"} : ""}>
                    <Col span={24}>
                        <Card style={{textAlign: 'center'}}>
                            <Statistic
                                title={`${t("shouldPay")}(USDT-TRC20)`}
                                value={order.amount}
                                precision={6}
                                valueStyle={{ color: '#cf1322' }}
                                className="currency-single"
                                prefix={<img src={USDT} alt="usdt"/>}
                            />
                        </Card>
                    </Col>
                </Row>
                <Row gutter={8} style={mode!=="rate" ? {display:"none"} : ""}>
                    <Col span={12}>
                        <Card>
                        <Statistic
                            title={`${t("order.currencyAmount")}(CNY)`}
                            value={order.currencyAmount}
                            valueStyle={{ color: '#3f8600' }}
                            prefix={<img src={CNY} alt="cny"/>}
                            className="currency"
                        />
                        </Card>
                    </Col>
                    <Col span={12}>
                        <Card>
                        <Statistic
                            title={`${t("shouldPay")}(USDT)`}
                            value={order.amount}
                            precision={3}
                            valueStyle={{ color: '#cf1322' }}
                            className="currency"
                            prefix={<img src={USDT} alt="usdt"/>}
                        />
                        </Card>
                    </Col>
                </Row>
                <Divider/>
                <QRCode value={ order.receiveWallet ? order.receiveWallet : ""} className="qrcode"/>
                <List
                    dataSource={detail}
                    renderItem={item => 
                        item.key==="rate" ? mode==="rate" ? (
                            <List.Item
                                actions={[item.extra]}
                                className="items"
                            >
                                <Skeleton title={false} loading={false} active>
                                    <List.Item.Meta
                                        title={item.title}
                                        description={<div className="value">{item.value} </div>}
                                    />
                                </Skeleton>
                            </List.Item>
                        ) : "" : getListItem(item)
                    }
                />
            </Card>
        </div>
    );
};

export default Till;