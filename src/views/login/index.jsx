import React, { useState } from "react";
import { Redirect } from "react-router-dom";
import { Form, Input, Button, message, Spin } from "antd";
import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { connect } from "react-redux";
import DocumentTitle from "react-document-title";
import "./index.less";
import { login, getUserInfo } from "@/store/actions";
import { useTranslation } from 'react-i18next';

const Login = (props) => {
  const { token, login, getUserInfo } = props;
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);

  const handleLogin = (username, password) => {
    setLoading(true);
    login(username, password)
      .then((data) => {
        message.success(t("login.success"));
        handleUserInfo(data.token);
      })
      .catch((error) => {
        setLoading(false);
        message.error(error);
      });
  };

  const handleUserInfo = (token) => {
    getUserInfo(token)
      .then((data) => { })
      .catch((error) => {
        message.error(error);
      });
  };

  const handleSubmit = (values) => {
    const { username, password } = values;
    handleLogin(username, password);
  };

  if (token) {
    return <Redirect to="/sys_user/dashboard" />;
  }
  return (
    <DocumentTitle title={t("login")}>
      <div className="login-container">
        <Form onFinish={handleSubmit} className="content">
          <div className="title">
            <h2>{t('login')}</h2>
          </div>
          <Spin spinning={loading} tip={t("login.load")}>
            <Form.Item
              name="username"
              rules={[
                {
                  required: true,
                  whitespace: true,
                  message: t("notice.account"),
                },
              ]}
            >
              <Input
                prefix={
                  <UserOutlined style={{ color: "rgba(0,0,0,.25)" }} />
                }
                placeholder={t("account")}
              />
            </Form.Item>
            <Form.Item
              name="password"
              rules={[
                {
                  required: true,
                  whitespace: true,
                  message: t("notice.password"),
                },
              ]}
            >
              <Input
                prefix={
                  <LockOutlined style={{ color: "rgba(0,0,0,.25)" }} />
                }
                type="password"
                placeholder={t("password")}
              />
            </Form.Item>
            <Form.Item>
              <Button
                type="primary"
                htmlType="submit"
                className="login-form-button"
              >
                {t('login')}
              </Button>
            </Form.Item>
          </Spin>
        </Form>
      </div>
    </DocumentTitle>
  );
};


export default connect((state) => state.user, { login, getUserInfo })(Login);
