import React from "react";
import { connect } from "react-redux";
import { Menu, Dropdown, Modal, Layout } from "antd";
import { logout, getUserInfo } from "@/store/actions";
import Hamburger from "@/components/Hamburger";
import BreadCrumb from "@/components/BreadCrumb";
import { CaretDownOutlined } from '@ant-design/icons';
import "./index.less";
import { useTranslation } from 'react-i18next';

const { Header } = Layout;

const LayoutHeader = (props) => {
  const { t } = useTranslation();
  const {
    token,
    sidebarCollapsed,
    logout,
    fixedHeader,
    account
  } = props;
  const handleLogout = (token) => {
    Modal.confirm({
      title: t("logout"),
      content: `${t("checkLogout")}？`,
      okText: t("confirm"),
      cancelText: t("cancel"),
      onOk: () => {
        logout(token);
      },
    });
  };
  const onClick = ({ key }) => {
    switch (key) {
      case "logout":
        handleLogout(token);
        break;
      default:
        break;
    }
  };
  const menu = (
    <Menu onClick={onClick}>
      <Menu.Item key="logout">{t("logout")}</Menu.Item>
    </Menu>
  );
  const computedStyle = () => {
    let styles;
    if (fixedHeader) {
      if (sidebarCollapsed) {
        styles = {
          width: "calc(100% - 80px)",
        };
      } else {
        styles = {
          width: "calc(100% - 200px)",
        };
      }
    } else {
      styles = {
        width: "100%",
      };
    }
    return styles;
  };
  return (
    <>
      {/* 这里是仿照antd pro的做法,如果固定header，
      则header的定位变为fixed，此时需要一个定位为relative的header把原来的header位置撑起来 */}
      {fixedHeader ? <Header /> : null}
      <Header
        style={computedStyle()}
        className={fixedHeader ? "fix-header" : ""}
      >
        <Hamburger />
        <BreadCrumb />
        <div className="right-menu">
          <div className="dropdown-wrap">
            <Dropdown overlay={menu}>
              <div>
                {account} &nbsp;
                <CaretDownOutlined style={{ color: "rgba(0,0,0,.3)" }} />
              </div>
            </Dropdown>
          </div>
        </div>
      </Header>
    </>
  );
};

const mapStateToProps = (state) => {
  return {
    ...state.app,
    ...state.user,
    ...state.settings,
  };
};
export default connect(mapStateToProps, { logout, getUserInfo })(LayoutHeader);
