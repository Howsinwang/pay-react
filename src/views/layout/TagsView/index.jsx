import React from "react";
import TagList from "./components/TagList";
import "./index.less";
import { useTranslation } from 'react-i18next';

const TagsView = () => {
  const { t } = useTranslation();
  return (
    <div className="tagsView-container">
      <TagList t={t}/>
    </div>
  );
};

export default TagsView;
