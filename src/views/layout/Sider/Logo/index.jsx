import React from "react";
import logo from "@/assets/images/logo.svg";
import "./index.less";
import { useTranslation } from 'react-i18next';


const Logo = () => {
  const { t } = useTranslation();
  return (
    <div className="sidebar-logo-container">
      <img src={logo} className="sidebar-logo" alt="logo" />
      <h1 className="sidebar-title">{t("backend")}</h1>
    </div>
  );
};

export default Logo;
