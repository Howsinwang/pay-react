import React, { useState,useEffect } from "react";
import { Menu } from "antd";
import IconSelector from "@/components/IconSelector";
import { Link, withRouter } from "react-router-dom";
import { Scrollbars } from "react-custom-scrollbars";
import { connect } from "react-redux";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import { addTag } from "@/store/actions";
import { getMenuItemInMenuListByProperty } from "@/utils";
import menuList from "@/config/menuConfig";
import "./index.less";
import { useTranslation } from 'react-i18next';

const SubMenu = Menu.SubMenu;
// 重新记录数组顺序
const reorder = (list, startIndex, endIndex) => {
  const result = Array.from(list);
  const [removed] = result.splice(startIndex, 1);
  result.splice(endIndex, 0, removed);
  return result;
};

const Meun = (props) => {
  const [menuTreeNode,setMenuTreeNode] = useState(null);
  const [openKey,setOpenKey] = useState([]);
  const { t } = useTranslation();

  useEffect(()=>{
    setMenuTreeNode(getMenuNodes(menuList));
    handleMenuSelect(openKey);
  },[])

  const filterMenuItem = (item) => {
    const { mRoles } = item;
    const { roles } = props;
    if (roles.includes(1) || !mRoles || mRoles.includes(roles[0])) {
      return true;
    } else if (item.children) {
      // 如果当前用户有此item的某个子item的权限
      return !!item.children.find((child) => roles.includes(child.role));
    }
    return false;
  };

  const getMenuNodes = (menuList) => {
    
    // 得到当前请求的路由路径
    const path = props.location.pathname;
    return menuList.reduce((pre, item) => {
      if (filterMenuItem(item)) {
        if (!item.children) {
          pre.push(
            <Menu.Item key={item.path}>
              <Link to={item.path}>
                {item.icon ? <IconSelector type={item.icon} /> : null}
                <span>{t(item.title)}</span>
              </Link>
            </Menu.Item>
          );
        } else {
          // 查找一个与当前请求路径匹配的子Item
          const cItem = item.children.find(
            (cItem) => path.indexOf(cItem.path) === 0
          );
          // 如果存在, 说明当前item的子列表需要打开
          if (cItem) {
            setOpenKey([...openKey, item.path]);
          }

          // 向pre添加<SubMenu>
          pre.push(
            <SubMenu
              key={item.path}
              title={
                <span>
                  {item.icon ? <IconSelector type={item.icon} /> : null}
                  <span>{t(item.title)}</span>
                </span>
              }
            >
              {getMenuNodes(item.children)}
            </SubMenu>
          );
        }
      }

      return pre;
    }, []);
  };

  const onDragEnd = (result) => {
    if (!result.destination) {
      return;
    }
    const _items = reorder(
      menuTreeNode,
      result.source.index,
      result.destination.index
    );
    setMenuTreeNode(_items);
  };

  const handleMenuSelect = ({ key = "/sys_user/dashboard" }) => {
    let menuItem = getMenuItemInMenuListByProperty(menuList, "path", key);
    props.addTag(menuItem);
  };

  const path = props.location.pathname;

  return (
    <div className="sidebar-menu-container">
        <Scrollbars autoHide autoHideTimeout={1000} autoHideDuration={200}>
          <DragDropContext onDragEnd={onDragEnd}>
            <Droppable droppableId="droppable">
              {(provided, snapshot) => (
                <div {...provided.droppableProps} ref={provided.innerRef}>
                  {menuTreeNode==null ? "" : menuTreeNode.map((item, index) => (
                    <Draggable
                      key={item.key}
                      draggableId={item.key}
                      index={index}
                    >
                      {(provided, snapshot) => (
                        <div
                          ref={provided.innerRef}
                          {...provided.draggableProps}
                          {...provided.dragHandleProps}
                        >
                          <Menu
                            mode="inline"
                            theme="dark"
                            onSelect={handleMenuSelect}
                            selectedKeys={[path]}
                            defaultOpenKeys={openKey}
                          >
                            {item}
                          </Menu>
                        </div>
                      )}
                    </Draggable>
                  ))}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </Scrollbars>
      </div>
  );
};

export default connect((state) => state.user, { addTag })(withRouter(Meun));
