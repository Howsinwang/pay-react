import React,{useCallback,useEffect,useState} from "react";
import { Row, Col } from "antd";
import errImg from "@/assets/images/404.png";
import "./index.less";
import { useTranslation } from 'react-i18next';

const OrderTimeout = (props) => {
  const { match:{params} } = props;
  const { t, i18n } = useTranslation();
  const [display,setDisplay]= useState("none");

  const changeLanguage = useCallback((lang) => {
    const changeLanguageHandler = () =>
      {
          i18n.changeLanguage(lang)
      }
      changeLanguageHandler();
  },[i18n]);


  useEffect(()=>{
      if ( params.lang!=null ){
          changeLanguage(params.lang);
      }
      setDisplay("block");
  },[changeLanguage,params]);

  return (
    <Row className="not-found">
      <Col span={12}>
        <img src={errImg} alt="404" />
      </Col>
      <Col span={12} className="right" style={{display:display}}>
        <h1>{t("notice")}</h1>
        <h2>{t("orderNotFound")}</h2>
      </Col>
    </Row>
  );
};

export default OrderTimeout;
