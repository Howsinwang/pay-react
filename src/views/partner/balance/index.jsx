import React, { useEffect, useState, useCallback } from "react";
import { connect } from "react-redux"
import { Table, Button, Card, Modal, Descriptions, DatePicker, Form, Input } from "antd";
import { getBalanceFlow } from "@/api/info";
import "./index.less"
import { useTranslation } from 'react-i18next';

const { RangePicker } = DatePicker;
const BalanceFlow = (props) => {
    const { t } = useTranslation();
    const { account } = props;
    const columns = [
        {
            title: t("operation"),
            dataIndex: 'remark'
        },
        {
            title: t("order.userOrderId"),
            dataIndex: 'userOrderId',
            render: userOrderId => userOrderId == null ? "" : `${userOrderId.substring(0, 15)}...`,
            width: '15%'
        },
        {
            title: t("order.userMemberId"),
            dataIndex: 'userMemberId'
        },
        {
            title: t("payment"),
            dataIndex: 'payment',
            render: payment => paintPayment(payment)
        },
        {
            title: t("balance"),
            dataIndex: 'balance'
        },
        {
            title: t("order.createdDate"),
            dataIndex: 'createdDate',
            render: createdDate => new Date(createdDate).toLocaleString(),
            defaultSortOrder: 'descend',
            sorter: (a, b) => (
                new Date(a.createdDate).getTime() - new Date(b.createdDate).getTime()
            )
        },
        {
            title: t("operation"),
            render: (record) => (
                <div>
                    <Button type="primary" size="small" className="btn-opt" onClick={() => showModal(record)}>{t("detail")}</Button>
                </div>
            )
        }
    ];
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [data, setData] = useState([]);
    const [pagination, setPagination] = useState({
        current: 1,
        pageSize: 10,
    });
    const [condition, setCondition] = useState({});
    const [loading, setLoading] = useState(false);
    const [record, setRecord] = useState({});
    const [result, setResult] = useState([]);

    const fetchData = useCallback(()=>{
        const fetchingData = (params) => {
            setLoading(true);
            if (data.length === 0) {
                getBalanceFlow().then((response) => {
                    setLoading(false);
                    setPagination({ ...params, pagination });
                    const records = response.data.map(record => {
                        return {
                            ...record,
                            key: record.id
                        }
                    });
                    setData(records);
                    setResult(records);
                });
            }
            else {
                setResult(data.filter(record =>
                    (condition.userOrderId ? (record.userOrderId ? record.userOrderId.includes(condition.userOrderId) : false) : true) &&
                    (condition.userMemberId ? (record.userMemberId ? record.userMemberId.includes(condition.userMemberId) : false) : true) &&
                    (condition.orderId ? (record.orderId ? record.orderId.includes(condition.orderId) : false) : true) &&
                    (condition.partnerId ? record.partnerId.includes(condition.partnerId) : true) &&
                    (condition.transactionId ? record.transactionId.includes(condition.transactionId) : true) &&
                    (condition.start ? condition.start.isSameOrBefore(record.createdDate, "day") : true) &&
                    (condition.end ? condition.end.isSameOrAfter(record.createdDate, "day") : true)
                ));
            }
            setLoading(false);
        };
        fetchingData();
    },[data,condition,pagination]);
    
    const showModal = (record) => {
        setRecord(record);
        setIsModalVisible(true);
    };

    const handleCancel = () => {
        setIsModalVisible(false);
    };

    const filterUserOrderIdChange = (e) => {
        let value = e.target.value;
        setCondition({
            ...condition,
            userOrderId: value
        });
    };

    const filterUserMemberIdChange = (e) => {
        let value = e.target.value;
        setCondition({
            ...condition,
            userMemberId: value
        });
    };

    const filterTxIdChange = (e) => {
        let value = e.target.value;
        setCondition({
            ...condition,
            transactionId: value
        })
    };
    const filterOrderId = (e) => {
        let value = e.target.value;
        setCondition({
            ...condition,
            orderId: value
        })
    };
    const filterPartnerId = (e) => {
        let value = e.target.value;
        setCondition({
            ...condition,
            partnerId: value
        })
    };
    const filterDateRangeChange = (value, dateString) => {
        setCondition({
            ...condition,
            start: value ? value[0] : null,
            end: value ? value[1] : null
        })
    };
    const paintPayment = (payment) => {
        return (
            <div>
                {payment > 0 ? <div style={{ color: "green" }}>+{payment}</div> : <div style={{ color: "red" }}>{payment}</div>}
            </div>
        );
    }

    const handleDownload = (type) => {
        import("@/lib/Export2Excel").then((excel) => {
            const tHeader = columns.filter(column=>column.dataIndex!==undefined).map(column=>column.title);
            tHeader.push("TxId");
            const filterVal = columns.filter(column=>column.dataIndex!==undefined).map(column=>column.dataIndex);
            filterVal.push("transactionId");
            const data = formatJson(filterVal, result.map(record=>{
                record.createdDate = new Date(record.createdDate).toLocaleString();
                return record;
            }));
            
            excel.export_json_to_excel({
                header: tHeader,
                data,
                filename: `${account}-${Date.now()}`,
                autoWidth: true,
                bookType: "xlsx",
            });
        });
    };

    const formatJson = (filterVal, jsonData) => {
        return jsonData.map(v => filterVal.map(j => v[j]))
    }

    useEffect(() => {
        fetchData(pagination);
    },[fetchData,pagination]);

    return (
        <div className="panel">
            <Card title={t("menu.balanceFlow")} bordered={true} extra={`${t("lastUpdatedDate")} ${new Date().toLocaleString()}`}>
                <Form layout="inline">
                    <Form.Item label={`${t("order.userOrderId")}:`}>
                        <Input onChange={filterUserOrderIdChange} />
                    </Form.Item>
                    <Form.Item label={`${t("order.userMemberId")}:`}>
                        <Input onChange={filterUserMemberIdChange} />
                    </Form.Item>
                    <Form.Item label="TxId:">
                        <Input onChange={filterTxIdChange} />
                    </Form.Item>
                    <Form.Item label={`${t("partner.partnerId")}:`} style={account!=="admin" ? {display:"none"} : {}}>
                        <Input onChange={filterPartnerId} />
                    </Form.Item>
                    <Form.Item label={`${t("order.orderId")}:`}>
                        <Input onChange={filterOrderId} />
                    </Form.Item>
                    <Form.Item label={`${t("order.createdDate")}:`}>
                        <RangePicker
                            onChange={filterDateRangeChange}
                        />
                    </Form.Item>
                    <Form.Item>
                        <Button onClick={handleDownload}>{t("export")}</Button>
                    </Form.Item>
                </Form>
                <br/>
                <Table
                    bordered
                    size="small"
                    columns={columns}
                    dataSource={result}
                    pagination={pagination}
                    loading={loading}
                />
            </Card>
            <Modal title="詳細資料" visible={isModalVisible} onCancel={handleCancel} width={700} footer={null}>
                <Descriptions
                    size="small"
                    bordered
                    column={{ xxl: 2, xl: 2, lg: 2, md: 2, sm: 2, xs: 2 }}
                >
                    <Descriptions.Item label={t("operation")} span={2}>{record.remark}</Descriptions.Item>
                    <Descriptions.Item label={t("partner.partnerId")} span={2}>{record.partnerId}</Descriptions.Item>
                    <Descriptions.Item label={t("order.orderId")} span={2}>{record.orderId}</Descriptions.Item>
                    <Descriptions.Item label={t("order.userOrderId")} span={2}>{record.userOrderId}</Descriptions.Item>
                    <Descriptions.Item label={t("order.userMemberId")}>{record.userMemberId}</Descriptions.Item>
                    <Descriptions.Item label={t("order.createdDate")}>{new Date(record.createdDate).toLocaleString()}</Descriptions.Item>
                    <Descriptions.Item label={t("payment")}>{paintPayment(record.payment)}</Descriptions.Item>
                    <Descriptions.Item label={t("balance")}>{record.balance}</Descriptions.Item>
                    <Descriptions.Item label={t("order.fromWallet")} span={2}>{record.fromWallet}</Descriptions.Item>
                    <Descriptions.Item label={t("order.toWallet")} span={2}>{record.toWallet}</Descriptions.Item>
                    <Descriptions.Item label="TxID" span={2}>{record.transactionId}</Descriptions.Item>
                </Descriptions>
            </Modal>
        </div>
    );
}

export default connect((state) => state.user)(BalanceFlow);