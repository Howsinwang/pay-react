import React from 'react';
import { Button, Modal, Form, Input, message } from "antd";
import { connect } from "react-redux";
import store from "@/store";
import { setUserInfo } from "@/store/actions";
import { updatePartnerExchangeRate } from '@/api/user';

const UsdtCny = (props) => {
    const {visible,onCancel,onFinish,user,t} = props;
    const handleOk = (values) => {
        updatePartnerExchangeRate(values).then((response)=>{
            const { msg } = response.data;
            user.partner.exchangeRate = values["exchangeRate"];
            store.dispatch(setUserInfo(user));
            
            onFinish(msg);
        }).catch((error) => {
            console.log(error);
            message.error(error.response.data.msg);
        });
    };
    return (
        <div>
            <Modal
                title={t("setUsdtCny")}
                visible={visible}
                onCancel={onCancel}
                width={400}
                footer={null}
            >
                <Form
                    onFinish={handleOk}
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 16 }}
                >
                    <Form.Item
                        label="CNY/USDT"
                        name="exchangeRate"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("usdtCnyNotice"),
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label={t("verifyCode")}
                        name="code"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("noticeVerifyCode"),
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{ offset: 18 }}
                    >
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="login-form-button"
                        >
                            {t("confirm")}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}
const mapStateToProps = (state) => {
    return {
      ...state,
      user: state.user,
    };
  };
  
  export default connect(mapStateToProps)(UsdtCny);