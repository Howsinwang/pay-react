import React from 'react';
import { Button, Modal, Form, Input, Row, Divider, message } from "antd";
import { connect } from "react-redux";
import { saveGoogleCode } from '@/api/user';
import QRCode from 'qrcode.react';

const GoogleAuth = (props) => {
    const {visible,onCancel,onFinish,user,googleSecret,setIsSetting,t} = props;
    const handleOk = (values) => {
        saveGoogleCode(values).then((response)=>{
            const { msg } = response.data;
            setIsSetting(true);
            onFinish(msg);
        }).catch((error) => {
            console.log(error.response);
            message.error(error.response.data.msg);
        });
    };

    return (
        <div>
            <Modal
                title={t("setGoogleAuth")}
                visible={visible}
                onCancel={onCancel}
                width={400}
                footer={null}
            >
                <Row justify="center">
                    <QRCode value={`otpauth://totp/${user.account}?secret=${googleSecret}`} />
                </Row>
                <Divider/>
                <Form
                    onFinish={handleOk}
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 16 }}
                >
                    <Form.Item
                        label={t("verifyCode")}
                        name="code"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("noticeVerifyCode"),
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{ offset: 18 }}
                    >
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="login-form-button"
                        >
                            {t("confirm")}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}
const mapStateToProps = (state) => {
    return {
      ...state,
      user: state.user,
    };
  };
  
  export default connect(mapStateToProps)(GoogleAuth);