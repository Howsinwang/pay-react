import React, { useEffect, useState } from "react";
import "./index.less"
import { connect } from "react-redux";
import { List, Button, Card, Row, Col, Skeleton, Descriptions, Divider, Modal, message } from "antd";
import ForgotModal from './forgot';
import NotifyUrl from './url';
import GoogleAuth from './google'
import UsdtCny from './usdtcny'
import WithdrawUsdt from './withdraw';
import DepositUsdt from './deposit';
import { getWalletInfo } from '@/api/info'
import { SyncOutlined } from '@ant-design/icons';
import { checkGoogleCode,getGoogleCode } from '@/api/user';
import { useTranslation } from 'react-i18next';

const PartnerInformation = (props) => {
  const { t } = useTranslation();
  const { user } = props;
  const { partner } = user;
  const [items, setItems] = useState([]);
  const [walletInfo,setWalletInfo] = useState({});
  const [isModalVisible, setIsModalVisible] = useState("");
  const [loading, setLoading] = useState(false);
  const [googleSecret,setGoogleSecret] = useState("");
  const [isSetting,setIsSetting] = useState();
  const showModal = (target) => {
    setIsModalVisible(target);
  };

  const setGoogleCode = (isReset) => {
    if ( isReset ) {
      Modal.confirm({
        title: t("confirmReset"),
        content:
          t("noticeResetGoogle"),
        okText: t("confirm"),
        cancelText: t("cancel"),
        onOk() {
          openSetModal();
        },
        onCancel() {
        },
      });
    }
    else {
      openSetModal();
    }
  };

  const openSetModal = async () => {
    await getGoogleCode().then((response)=>{
      const data = response.data.data;
      setGoogleSecret(data);
      showModal("google");
    }).catch((error) => {
      console.log(error);
    });
  };

  const handleCancel = () => {
    setIsModalVisible("");
  };

  const changeView = (msg) => {
    message.success(msg);
    setIsModalVisible("");
  };

  useEffect(() => {
    setLoading(true);
    getWalletInfo().then((response)=>{
      setWalletInfo(response.data);
      setLoading(false);
    });
    checkGoogleCode().then((response)=>{
      setItems([
        {
          key: "password",
          title: t("resetPassword"),
          content: t("resetPasswordMessage"),
          button: <Button type="primary" size="small" onClick={() => showModal("password")}>{t("setting")}</Button>
        },
        {
          key: "google",
          title: t("googleAuth"),
          content: t("googleAuthMessage"),
          button: response.data.data ? /*<Button type="danger" size="small" onClick={() => setGoogleCode(true)}>重設</Button>*/ "" :
          <Button type="primary" size="small" onClick={() => setGoogleCode()}>{t("setting")}</Button>
        },
        {
          key: "rechargeNotifyUrl",
          title: t("setCallbackUrl"),
          content: t("setCallbackUrlMessage"),
          button: <Button type="primary" size="small" onClick={() => showModal("rechargeNotifyUrl")}>{t("setting")}</Button>
        },
        {
          key: "usdtCny",
          title: t("setUsdtCny"),
          content: t("setUsdtCnyMessage"),
          button: <Button type="primary" size="small" onClick={() => showModal("usdtCny")}>{t("setting")}</Button>
        },
      ]);
      setIsSetting(response.data.data);
    });
  }, [partner,isSetting]);

  const walletTitle = () => {
    if (loading){
      return (<div>
        {t("wallet")} <SyncOutlined spin/>
      </div>)
    }
    else {
      return t("wallet");
    }
    
  };

  const rechargeTitle = () => {
    if (loading){
      return (<div>
        {t("depositInfo")} <SyncOutlined spin/>
      </div>)
    }
    else {
      return t("depositInfo");
    }
    
  };

  return (
    <div>
      <Row>
        <Col xs={16} sm={16} md={16} lg={16} xl={16}>
          <div className="panel-left">
            <Card title={null} bordered={true}>
              <Descriptions
                  title={t("user.account")}
                  size="small"
                  column={{ xxl: 4, xl: 4, lg: 4, md: 4, sm: 4, xs: 4 }}
                >
                <Descriptions.Item label={t("user.account")} span={1}>{user.account}</Descriptions.Item>
                <Descriptions.Item label={t("user.name")} span={1}>{user.name}</Descriptions.Item>
                <Descriptions.Item label={t("partner.partnerId")} span={2}>{partner.partnerId}</Descriptions.Item>
              </Descriptions>
              <Divider/>
              <Descriptions
                  title={rechargeTitle()}
                  size="small"
                  column={{ xxl: 4, xl: 4, lg: 4, md: 4, sm: 4, xs: 4 }}
                >
                <Descriptions.Item label={t("chain")} span={1}>Tron</Descriptions.Item>
                <Descriptions.Item label={t("coinType")} span={1}>USDT</Descriptions.Item>
                <Descriptions.Item label={t("transferRate")} span={1}>{partner.depositFee}</Descriptions.Item>
                <Descriptions.Item label={t("platformRate")} span={1}>{partner.depositBasicFee}</Descriptions.Item>
                <Descriptions.Item label={t("partner.exchangeRate")} span={1}>{partner.exchangeRate}</Descriptions.Item>
                <Descriptions.Item label={t("partner.rechargeNotifyUrl")} span={2}>{partner.rechargeNotifyUrl == null ? t("notSetting") : partner.rechargeNotifyUrl}</Descriptions.Item>
              </Descriptions>
              <Divider/>
              <Descriptions
                  title={t("withdrawInfo")}
                  size="small"
                  column={{ xxl: 4, xl: 4, lg: 4, md: 4, sm: 4, xs: 4 }}
                >
                <Descriptions.Item label={t("chain")} span={1}>Tron</Descriptions.Item>
                <Descriptions.Item label={t("coinType")} span={1}>USDT</Descriptions.Item>
                <Descriptions.Item label={t("transferRate")} span={1}>{partner.payFee}</Descriptions.Item>
                <Descriptions.Item label={t("platformRate")} span={1}>{partner.payBasicFee}</Descriptions.Item>
              </Descriptions>
              <Divider/>
              <Descriptions
                  title={walletTitle()}
                  size="small"
                  column={{ xxl: 2, xl: 2, lg: 2, md: 2, sm: 2, xs: 2 }}
                  extra={ 
                    <div>
                      <Button type="primary" size="small" onClick={()=>showModal("depositUsdt")}>{t("deposit")}</Button>
                      <Divider type="vertical" />
                      <Button type="primary" size="small" onClick={()=>showModal("withdrawUsdt")}>{t("withdraw")}</Button>
                    </div>
                  }
                >
                <Descriptions.Item label={t("partner.payWallet")} span={1}>{partner.payWallet}</Descriptions.Item>
                <Descriptions.Item label={t("partner.usdt")} span={1}>{partner.usdt}</Descriptions.Item>
                <Descriptions.Item label={t("lastUpdatedDate")} span={1}>{new Date(walletInfo.current).toLocaleString()}</Descriptions.Item>
              </Descriptions>
            </Card>
          </div>
        </Col>
        <Col xs={8} sm={8} md={8} lg={8} xl={8}>
          <div className="panel-right">
            <Card title={t("securitySetting")} bordered={true}>
              <List
                itemLayout="horizontal"
                grid={{
                  gutter: 16,
                  xs: 1,
                  sm: 1,
                  md: 1,
                  lg: 1,
                  xl: 1,
                  xxl: 1,
                }}
                dataSource={items}
                renderItem={item => (
                  <List.Item
                    actions={[item.button]}
                  >
                    <Skeleton title={false} loading={false} active>
                      <List.Item.Meta
                        title={item.title}
                        description={<div className="notice">{item.content} </div>}
                      />
                    </Skeleton>
                  </List.Item>
                )}
              />
            </Card>
          </div>
        </Col>
      </Row>
      <ForgotModal 
        visible={isModalVisible==="password"}
        onCancel={handleCancel}
        onFinish={changeView}
        t={t}
      />
      <NotifyUrl 
        visible={isModalVisible==="rechargeNotifyUrl"}
        onCancel={handleCancel}
        onFinish={changeView}
        t={t}
      />
      <GoogleAuth 
        visible={isModalVisible==="google"}
        onCancel={handleCancel}
        onFinish={changeView}
        googleSecret={googleSecret}
        setIsSetting={setIsSetting}
        t={t}
      />
      <UsdtCny 
        visible={isModalVisible==="usdtCny"}
        onCancel={handleCancel}
        onFinish={changeView}
        googleSecret={googleSecret}
        setIsSetting={setIsSetting}
        t={t}
      />
      <WithdrawUsdt 
        visible={isModalVisible==="withdrawUsdt"}
        onCancel={handleCancel}
        onFinish={changeView}
        googleSecret={googleSecret}
        setIsSetting={setIsSetting}
        t={t}
      />
      <DepositUsdt 
        visible={isModalVisible==="depositUsdt"}
        onCancel={handleCancel}
        onFinish={changeView}
        walletAddress={partner.depositWallet}
        t={t}
      />
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    ...state,
    user: state.user,
  };
};

export default connect(mapStateToProps)(PartnerInformation);