import React from 'react';
import { Button, Modal, Form, Input, message } from "antd";
import { changePassword } from '@/api/user';

const ForgotModal = (props) => {
    const {visible,onCancel,onFinish,t} = props;
    const handleOk = (values) => {
        changePassword(values).then((response)=>{
            const { msg } = response.data;
            onFinish(msg);
        }).catch((error) => {
            console.log(error.response);
            message.error(error.response.data.msg);
        });
    };

    return (
        <div>
            <Modal
                title={t("resetPassword")}
                visible={visible}
                onCancel={onCancel}
                width={400}
                footer={null}
            >
                <Form
                    onFinish={handleOk}
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 16 }}
                >
                    <Form.Item
                        label={t("account")}
                        name="password"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("notice.password"),
                            },
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item
                        label={t("typeAgain")}
                        name="password2"
                        dependencies={['password']}
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("notice.password"),
                            },
                            ({ getFieldValue }) => ({
                                validator(_, value) {
                                    if (!value || getFieldValue('password') === value) {
                                        return Promise.resolve();
                                    }
                                    return Promise.reject(new Error(t("passwordDiff")));
                                },
                            })
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>
                    <Form.Item
                        label={t("verifyCode")}
                        name="code"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("noticeVerifyCode"),
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{ offset: 18 }}
                    >
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="login-form-button"
                        >
                            {t("confirm")}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}

export default ForgotModal;