import React from 'react';
import { Button, Modal, Form, Input, message } from "antd";
import { connect } from "react-redux";
import store from "@/store";
import { setUserInfo } from "@/store/actions";
import { changeCollectWallet } from '@/api/user';

const CollectWalletModal = (props) => {
    const {visible,onCancel,onFinish,user} = props;
    const handleOk = (values) => {
        changeCollectWallet(values).then((response)=>{
            const { msg } = response.data;
            user.partner.trcCollectWallet = values["trcCollectWallet"];
            store.dispatch(setUserInfo(user));
            onFinish(msg);
        }).catch((error) => {
            console.log(error);
            message.error(error.response.data.msg);
        });
    };
    return (
        <div>
            <Modal
                title="設定歸集錢包"
                visible={visible}
                onCancel={onCancel}
                width={400}
                footer={null}
            >
                <Form
                    onFinish={handleOk}
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 16 }}
                >
                    <Form.Item
                        label="錢包地址"
                        name="trcCollectWallet"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: "請輸入錢包地址",
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="驗證碼"
                        name="code"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: "請輸入Google驗證碼",
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{ offset: 18 }}
                    >
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="login-form-button"
                        >
                            確認
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
      ...state,
      user: state.user,
    };
};
  
export default connect(mapStateToProps)(CollectWalletModal);