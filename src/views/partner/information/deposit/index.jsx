import React from 'react';
import { Button, Modal, Row, Divider, message } from "antd";
import QRCode from 'qrcode.react';
import {CopyToClipboard} from 'react-copy-to-clipboard';

const DepositUsdt = (props) => {
    const {visible,onCancel,walletAddress,t} = props;

    const onCopy = () => {
        message.success(t("copySuccess"));
    };

    return (
        <div>
            <Modal
                title={t("payWalletQRCode")}
                visible={visible}
                onCancel={onCancel}
                width={400}
                footer={null}
            >
                <Row justify="center">
                    <QRCode value={walletAddress} />
                    <Divider/>
                    {walletAddress}
                    <Divider type="vertical"/>
                    <CopyToClipboard onCopy={onCopy} text={walletAddress}>
                        <Button type="primary" size="small">{t("copy")}</Button>
                    </CopyToClipboard>
                </Row>
            </Modal>
        </div>
    );
}
  
  export default DepositUsdt;