import React,{useState,useEffect} from 'react';
import { Button, Modal, Form, Input, message } from "antd";
import { connect } from "react-redux";
import store from "@/store";
import { setUserInfo } from "@/store/actions";
import { partnerWithdraw } from '@/api/user';

const WithdrawUsdt = (props) => {
    const [isDisable,setIsDisable] = useState(false);
    const {visible,onCancel,onFinish,user,t} = props;
    const handleOk = (values) => {
        setIsDisable(true);
        partnerWithdraw(values).then((response)=>{
            const { msg } = response.data;
            user.partner.usdt = response.data.data;
            store.dispatch(setUserInfo(user));
            setIsDisable(false);
            onFinish(msg);
        }).catch((error) => {
            console.log(error);
            message.error(error.response.data.msg);
        });
    };
    return (
        <div>
            <Modal
                title={t("withdraw")}
                visible={visible}
                onCancel={onCancel}
                width={400}
                footer={null}
            >
                <Form
                    onFinish={handleOk}
                    labelCol={{ span: 6 }}
                    wrapperCol={{ span: 16 }}
                >
                    <Form.Item
                        label={t("payment")}
                        name="usdt"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("paymentNotice"),
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label={t("receiveWallet")}
                        name="targetAddress"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("noticeReceiveWallet"),
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label={t("verifyCode")}
                        name="code"
                        rules={[
                            {
                                required: true,
                                whitespace: true,
                                message: t("noticeVerifyCode"),
                            }
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        wrapperCol={{ offset: 18 }}
                    >
                        <Button
                            type="primary"
                            htmlType="submit"
                            className="login-form-button"
                            disabled={isDisable}
                        >
                            {t("confirm")}
                        </Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    );
}
const mapStateToProps = (state) => {
    return {
      ...state,
      user: state.user,
    };
  };
  
  export default connect(mapStateToProps)(WithdrawUsdt);