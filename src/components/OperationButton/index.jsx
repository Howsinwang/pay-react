import React from "react";
import { connect } from "react-redux";
import { Button, Divider } from "antd";
import { useTranslation } from 'react-i18next';
import { callbackOrder } from "@/api/order";

const OperationButton = (props) => {
    const { operation: { detail, callback, replenish, reverse } , order, user } = props;
    const { t } = useTranslation();

    const sendRequest = () => {
        callbackOrder({orderId:order.orderId}).then(resp => {
            callback();
        })
    };

    const buttons = {
        detail : {
            priority: [1,2],
            instance: (
                <Button 
                    type="primary"
                    size="small"
                    onClick={() => detail(order)} 
                    style={{
                        background: "#1da57a",
                        borderColor: "#1da57a",
                        fontSize: "smaller"
                    }}
                >
                    {t("detail")}
                </Button>
            )
        },
        callback: {
            priority: [1,2],
            instance: (
                <Button 
                    type="primary" 
                    size="small"
                    onClick={() => sendRequest(order)} 
                    style={{
                        fontSize: "smaller"
                    }}
                    danger
                >
                    {t("callback")}
                </Button>
            )
        },
        replenish : {
            priority: [1,2],
            instance: (
                <Button
                    type="primary" 
                    size="small"
                    onClick={() => replenish(order)} 
                    style={{
                        background: "#0373fc",
                        borderColor: "#0373fc",
                        fontSize: "smaller"
                    }}
                >
                    {t("replenishOrder")}
                </Button>
            )
        },
        reverse : {
            priority: [1],
            instance: (
                <Button
                    type="primary" 
                    size="small"
                    onClick={() => reverse(order)} 
                    style={{
                        background: "#19191a",
                        borderColor: "#19191a",
                        fontSize: "smaller"
                    }}
                >
                    {t("reverseOrder")}
                </Button>
            )
        },
    }
    
    const isButtonExist = (name) => props.operation[name] && user.roles.some( role  => buttons[name].priority.some( priority => priority===role) );
    const getDivider = () => <Divider type="vertical" />

    const getButtonGroupByStatus = () => {
        switch (order.status) {
            case -1: return (
                <div>
                    { isButtonExist("detail") && buttons["detail"].instance }
                    { isButtonExist("callback") && <>{getDivider()} {buttons["callback"].instance}</> }
                    { isButtonExist("reverse") && <>{getDivider()} {buttons["reverse"].instance}</> }
                </div>
            )
            case -2: return (
                <div>
                    { isButtonExist("detail") && buttons["detail"].instance }
                    { isButtonExist("replenish") && <>{getDivider()} {buttons["replenish"].instance}</> }
                </div>
            )
            case -3: return (
                <div>
                    { isButtonExist("detail") && buttons["detail"].instance }
                    { isButtonExist("replenish") && <>{getDivider()} {buttons["replenish"].instance}</> }
                </div>
            )
            case 2:  return (
                <div>
                    { isButtonExist("detail") && buttons["detail"].instance }
                    { isButtonExist("reverse") && <>{getDivider()} {buttons["reverse"].instance}</> }
                </div>
            )
            default: return isButtonExist("detail") && buttons["detail"].instance;
        }
    };

    return getButtonGroupByStatus();
};

const mapStateToProps = (state) => {
    return {
      ...state,
      user: state.user,
    };
};
  
export default connect(mapStateToProps)(OperationButton);