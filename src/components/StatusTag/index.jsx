import React from "react";
import { Tag } from "antd";
import { useTranslation } from 'react-i18next';
import { SyncOutlined, CheckCircleOutlined, CloseCircleOutlined, FieldTimeOutlined, StopOutlined, PlusCircleOutlined } from '@ant-design/icons';
const StatusTag = (props) => {
    const { status } = props;
    const { t } = useTranslation();
    const getTagByStatus = () => {
        switch (status) {
            case 0: return (
                <Tag color="green">
                    <PlusCircleOutlined />&nbsp;
                    {t("status.new")}
                </Tag>
            );
            case 1: return (
                <Tag color="blue">
                    <SyncOutlined spin />&nbsp;
                    {t("status.process")}
                </Tag>
            );
            case 2: return (
                <Tag color="green">
                    <CheckCircleOutlined />&nbsp;
                    {t("status.success")}
                </Tag>
            );
            case -1: return (
                <Tag color="red">
                    <CloseCircleOutlined />&nbsp;
                    {t("status.fail")}
                </Tag>
            );
            case -2: return (
                <Tag color="red">
                    <FieldTimeOutlined />&nbsp;
                    {t("status.timeout")}
                </Tag>
            );
            case -3: return (
                <Tag color="red">
                    <StopOutlined />&nbsp;
                    {t("status.cancel")}
                </Tag>
            );
            case -4: return (
                <Tag color="red">
                    <StopOutlined />&nbsp;
                    {t("status.reverse")}
                </Tag>
            );
            default: return "";
        }
    }
    return getTagByStatus();
};

export default StatusTag;
