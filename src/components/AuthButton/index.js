import React from "react";
import { connect } from "react-redux";
import { Button } from "antd";

const AuthButton = (props) => {
    const { privileges, user, name, type, size, onClick, style } = props;

    return (
        <>
            { 
                user.roles.some( role  => privileges.some( privilege => privilege===role) ) && 
                <Button 
                    type={type}
                    size={size}
                    onClick={onClick}
                    style={style}
                >
                    {name}
                </Button>
            }
        </>
    );
};

const mapStateToProps = (state) => {
    return {
      ...state,
      user: state.user,
    };
};
  
export default connect(mapStateToProps)(AuthButton);