import {
    HomeOutlined,
    ExportOutlined,
    ImportOutlined,
    MoneyCollectOutlined,
    CheckOutlined,
    SettingOutlined,
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    QuestionOutlined
} from '@ant-design/icons';
import * as PropTypes from 'prop-types';
import React from 'react';

  const IconSelector = ({ type, ...rest }) => {
    const getIcon = (iconType) => ({
      HomeOutlined: <HomeOutlined {...rest} />,
      ExportOutlined: <ExportOutlined {...rest} />,
      ImportOutlined: <ImportOutlined {...rest} />,
      MoneyCollectOutlined: <MoneyCollectOutlined {...rest} />,
      CheckOutlined: <CheckOutlined {...rest} />,
      SettingOutlined: <SettingOutlined {...rest} />,
      MenuUnfoldOutlined: <MenuUnfoldOutlined {...rest} />,
      MenuFoldOutlined: <MenuFoldOutlined {...rest} />,
    }[iconType]);
  
    return getIcon(type) || <QuestionOutlined {...rest} />;
  };
  
  IconSelector.propTypes = {
    type: PropTypes.string,
  };
  
  export default IconSelector;