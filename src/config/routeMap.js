import Loadable from 'react-loadable';
import Loading from '@/components/Loading'
const Error404 = Loadable({loader: () => import(/*webpackChunkName:'Error404'*/'@/views/error/404'),loading: Loading});
const OrderTimeout = Loadable({loader: () => import(/*webpackChunkName:'Error404'*/'@/views/error/order_timeout'),loading: Loading});
const UserDashboard = Loadable({loader: () => import(/*webpackChunkName:'Dashboard'*/'@/views/sys_user'),loading: Loading});
const Recharge = Loadable({loader: () => import(/*webpackChunkName:'Recharge'*/'@/views/recharge'),loading: Loading});
const GamblingOrder = Loadable({loader: () => import(/*webpackChunkName:'Recharge'*/'@/views/gambling/order'),loading: Loading});
const GamblingWallet = Loadable({loader: () => import(/*webpackChunkName:'Recharge'*/'@/views/gambling/wallet'),loading: Loading});
const Withdraw = Loadable({loader: () => import(/*webpackChunkName:'Recharge'*/'@/views/withdraw'),loading: Loading});
const Deposit = Loadable({loader: () => import(/*webpackChunkName:'Recharge'*/'@/views/deposit'),loading: Loading});
const SysUser = Loadable({loader: () => import(/*webpackChunkName:'Recharge'*/'@/views/user'),loading: Loading});
const PartnerInformation = Loadable({loader: () => import(/*webpackChunkName:'Recharge'*/'@/views/partner/information'),loading: Loading});
const Balance = Loadable({loader: () => import(/*webpackChunkName:'Recharge'*/'@/views/partner/balance'),loading: Loading});

export default [
  { path: "/error/404", component: Error404 },
  { path: "/error/timeout", component: OrderTimeout },
  { path: "/sys_user/dashboard", component: UserDashboard, mRoles: [1,2,3,4] },
  { path: "/recharge/record", component: Recharge, mRoles: [1,2,3,4] },
  { path: "/gambling/record", component: GamblingOrder, mRoles: [1,4] },
  { path: "/gambling/wallet", component: GamblingWallet, mRoles: [1,4] },
  { path: "/withdraw/record", component: Withdraw, mRoles: [1,2,3,4] },
  { path: "/deposits/record", component: Deposit, mRoles: [1,2,3,4] },
  { path: "/system/user", component: SysUser, mRoles: [1,2,4] },
  { path: "/partner/information", component: PartnerInformation, mRoles: [1,2,4] },
  { path: "/partner/balance", component: Balance, mRoles: [1,2,3,4] },
];
