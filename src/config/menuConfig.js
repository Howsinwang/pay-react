const menuList = [
  {
    title: "menu.partner",
    path: "/dashboard",
    icon: "HomeOutlined",
    mRoles: [1,2,3,4],
    children: [
      {
        title: "menu.information",
        path: "/sys_user/dashboard",
        mRoles: [1,2,3,4]
      },
      {
        title: "menu.partnerInfo",
        path: "/partner/information",
        mRoles: [1,2,4]
      },
      {
        title: "menu.balanceFlow",
        path: "/partner/balance",
        mRoles: [1,2,3,4]
      }
    ],
  },
  {
    title: "menu.deposit",
    path: "/recharge",
    icon: "ExportOutlined",
    mRoles:[1,2,3,4],
    children: [
      {
        title: "rechargeOrder",
        path: "/recharge/record",
        mRoles: [1,2,3,4]
      },
      {
        title: "depositOrder",
        path: "/deposits/record",
        mRoles: [1,2,3,4]
      }
    ]
  },
  {
    title: "menu.gambling",
    path: "/gambling",
    icon: "ExportOutlined",
    mRoles:[1,4],
    children: [
      {
        title: "menu.gamblingOrder",
        path: "/gambling/record",
        mRoles: [1,4]
      },
      {
        title: "menu.gamblingWallet",
        path: "/gambling/wallet",
        mRoles: [1,4]
      }
    ]
  },
  {
    title: "menu.withdraw",
    path: "/withdraw",
    icon: "ImportOutlined",
    mRoles:[1,2,3,4],
    children: [
      {
        title: "withdrawOrder",
        path: "/withdraw/record",
        mRoles: [1,2,3,4]
      }
    ]
  },
  {
    title: "menu.systemManage",
    path: "/system",
    icon: "SettingOutlined",
    mRoles:[1,2,4],
    children: [
      {
        title: "menu.userManage",
        path: "/system/user",
        mRoles: [1,2,4]
      },
    ]
  }
];
export default menuList;
