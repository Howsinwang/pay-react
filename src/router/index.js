import React from "react";
import { HashRouter, Route, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { getUserInfo } from "@/store/actions";
import Layout from "@/views/layout";
import Login from "@/views/login";
import OrderTimeout from "@/views/error/order_timeout";
import Till from "@/views/till";
import Develop from "@/views/develop";
class Router extends React.Component {
  render() {
    const { token, roles, getUserInfo } = this.props;
    return (
      <HashRouter>
        <Switch>
          <Route exact path="/dev" component={Develop} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/error/timeout/:lang" component={OrderTimeout} />
          <Route path="/deposit" component={Till} />
          <Route
            path="/"
            render={() => {
              if (!token) {
                return <Redirect to="/login" />;
              } else {
                if (roles.length!==0) {
                  return <Layout />;
                } else {
                  getUserInfo(token).then(() => <Layout />);
                }
              }
            }}
          />
        </Switch>
      </HashRouter>
    );
  }
}

export default connect((state) => state.user, { getUserInfo })(Router);
